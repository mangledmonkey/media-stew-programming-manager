<?php
/*
Plugin Name: Media Stew Programming Manager
Plugin URI:
Description: Manage and schedule episodic video content releases with social features. Relies on Twitter Bootstrap in theme.
Version: 0.3
Author: Brandon West
Author URI: http://mangledmonkeymedia.com
License: undetermined

	Copyright 2012  Brandon West (email : brandon@mangledmonkeymedia.com )

	EXAMPLE:
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License, version 2, as 
	published by the Free Software Foundation.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// don't load directly
if (!function_exists('is_admin')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

// Pre-2.6 compatibility
if ( ! defined( 'WP_CONTENT_URL' ) )
      define( 'WP_CONTENT_URL', get_option( 'siteurl' ) . '/wp-content' );
if ( ! defined( 'WP_CONTENT_DIR' ) )
      define( 'WP_CONTENT_DIR', ABSPATH . 'wp-content' );


define( 'MSPM_URL', WP_PLUGIN_URL . '/media-stew-programming-manager' );
define( 'MSPM_DIR', WP_PLUGIN_DIR . '/media-stew-programming-manager' );
define( 'MSPM_PHP_DIR', WP_PLUGIN_DIR . '/media-stew-programming-manager/php' );
define( 'MSPM_IMG_DIR', WP_PLUGIN_DIR . '/media-stew-programming-manager/img');

if (!class_exists("Media_Stew_Programming_Manager")) :
	
class Media_Stew_Programming_Manager {
	var $add_show, $add_episode, $manage;
	var $mspm_options, $mspm_options_pages, $mspm_shows_page, $mspm_episodes_page;


	function Media_Stew_Programming_Manager() {
		global $wp_rewrite;
		$version = get_bloginfo('version');
		$this->debug = FALSE;
		
		require(MSPM_PHP_DIR.'/mspm-permalinks.php');
		if (class_exists("Media_Stew_Programming_Manager_Permalink")) {
			$this->permalink_obj = new Media_Stew_Programming_Manager_Permalink();
		}
//		require(MSPM_PHP_DIR.'/mspm-taxonomies.php');
//		if (class_exists('Media_Stew_Programming_Manager_Taxonomies')) {
//			$this->taxonomies = new Media_Stew_Programming_Manager_Taxonomies();
//		}
		require(MSPM_PHP_DIR.'/mspm-meta-boxes.php');
		if (class_exists('Media_Stew_Programming_Manager_Meta_Boxes')) {
			$this->meta_boxes = new Media_Stew_Programming_Manager_Meta_Boxes();
		}
		require(MSPM_PHP_DIR.'/mspm-shortcodes.php');
		if (class_exists('Media_Stew_Programming_Manager_Shortcodes')) {
			$this->shortcodes = new Media_Stew_Programming_Manager_Shortcodes();
		}

		if (is_admin()) {
			require(MSPM_PHP_DIR . '/mspm-helper.php');
			if (class_exists("Media_Stew_Programming_Manager_Helper")) {
				$this->helper = new Media_Stew_Programming_Manager_Helper();
				add_action('admin_menu', array(&$this->helper,'mspm_add_pages'));
			}
		}

		add_action('admin_init', array(&$this, 'mspm_admin_init'));
		add_action('admin_init', array(&$this, 'mspm_init_options'));
		add_action('init', array(&$this,'mspm_init_general') );
		register_activation_hook( __FILE__, array(&$this,'activate' ) );

		register_deactivation_hook( __FILE__, array(&$this,'deactivate' ) );
		
		

		
			
	}

	function activate() {
		//global $wpdb;

		$this->_activate();
	}

	function deactivate() {
		global $wp_rewrite;
		$wp_rewrite->flush_rules();
	}

	function _activate() {
		$this->mspm_init_options();
		// Activate Shows/Episodes permalink
		$this->permalink_obj->mspm_activate();
		//$this->taxonomies->mspm_activate();
	}

	function mspm_init_options() {

		register_setting('mspm_general_options', 'mspm_options', array(&$this->helper,'mspm_sanitize_options'));
	}

	function mspm_admin_init() {


		// Add to the admin_init action hook
//		add_filter('current_screen', 'my_current_screen' );
 
		function my_current_screen($screen) {

			if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) return $screen;
			print_r($screen);
			return $screen;
		}
		

		$this->helper->mspm_admin_init();
//		$this->taxonomies->mspm_admin_init();
		$this->meta_boxes->mspm_admin_init();

	}


	function mspm_init_general(){
		$this->permalink_obj->mspm_init();
		$this->shortcodes->mspm_init();

		if (is_admin()) return;
	
	}






} // End Class MediaStewProgrammingManager

endif;

// instance the class as a new variable
if(class_exists("Media_Stew_Programming_Manager")) {
	$mspm = new Media_Stew_Programming_Manager();
}




















?>
