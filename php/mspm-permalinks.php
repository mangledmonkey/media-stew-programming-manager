<?php
// don't load directly
if (!function_exists('is_admin')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}


if (!class_exists("Media_Stew_Programming_Manager_Permalink")) :

class Media_Stew_Programming_Manager_Permalink {

	function mspm_activate() {
		register_activation_hook( __FILE__, array(&$this, 'mspm_rewrite_flush'));
	}

	function mspm_init() {
		global $wp_rewrite;
	
		// custom mspm series->episode permalink structure
		$series_structure = 'shows/%series%';
		$episode_structure = 'shows/%episode_parent%/%episode%';
		$wp_rewrite->add_permastruct('series', $series_structure, false);
		$wp_rewrite->add_permastruct('episode', $episode_structure, false);
		add_filter('rewrite_rules_array', array(&$this, 'mspm_rewrite_rules'));
		add_filter('post_type_link', array(&$this, 'mspm_filter_post_type_link'), 10,2);

		$this->mspm_register_series_object();
		$this->mspm_register_episode_object();


	}

	// ********** MODIFY REWRITE RULES ********** //

	function mspm_rewrite_rules($rules) {
		$newRules = array();
		$newRules['shows/([^/]+)/([^/]+)/?$'] = 'index.php?episode=$matches[2]';
		$newRules['shows/([^/]+)/?$'] = 'index.php?series=$matches[1]';
		
		return array_merge($newRules, $rules);
	}

	function mspm_filter_post_type_link($link, $post) {
		if ($post->post_type != 'episode')
			return $link;

		$post_parent = get_post($post->post_parent);
		if ($series = $post_parent->post_name)
			$link = str_replace('%episode_parent%',$series, $link);

		return $link;
	}

	// ******** REGISTER CUSTOM POST TYPES ********* //
	function mspm_register_series_object($name = 'Series') {
		$labels = array(
			'name' => _x('Series', 'post type general name'),
			'singular_name' => _x('Series', 'post type singular name'),
			'add_new' => _x('Add New', 'series'),
			'add_new_item' => __("Add New Series"),
			'edit_item' => __('Edit Series'),
			'new_item' => __('New Series'),
			'view_item' => __('View Series'),
			'search_items' => __('Search Series'),
			'not_found' => __('No series found'),
			'not_found_in_trash' => __('No series found in Trash'),
			'parent_item_colon' => '',
			'menu_name' => 'Series'
		);
		$args = array(
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => 'mspm',
			//'menu_icon' => MSPM_IMG_DIR.'/post-type-show.png',
			'publicly_queryable' => true,
			'query_var' => 'series',
			'capability_type' => 'page',
			'hierarchical' => true,
			'menu_position' => 10,
			//'rewrite' => array(
			//	'slug' => 'shows',
			//	'with_front' => false
			//),
			'rewrite' => false,
			'taxonomies' => array('category', 'post_tag'),
			'supports' => array('title','editor','comments','thumbnail')
		);	
		register_post_type('series',$args);
	}

	function mspm_register_episode_object($name = 'Episode') {
		$labels = array(
			'name' => _x('Episodes', 'post type general name'),
			'singular_name' => _x('Episode', 'post type singular name'),
			'add_new' => _x('Add New', 'episode'),
			'add_new_item' => __("Add New Episode"),
			'edit_item' => __('Edit Episode'),
			'new_item' => __('New Episode'),
			'view_item' => __('View Episode'),
			'search_items' => __('Search Episodes'),
			'not_found' => __('No episodes found'),
			'not_found_in_trash' => __('No episodes found in Trash'),
			'parent_item_colon' => 'Series:',
			'menu_name' => 'Episodes',
			'parent' => 'Series'
		);
		$args = array(
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => 'mspm',
			//'menu_icon' => MSPM_IMG_DIR.'/post-type-show.png',
			'publicly_queryable' => true,
			'query_var' => 'episode', 
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => 15,
		//	'rewrite' => array(
		//		'slug' => 'shows',
		//		'with_front' => false
		//	),
			'rewrite' => false,
			'taxonomies' => array('category', 'post_tag'),
			'supports' => array('title','editor','comments','thumbnail')
		);	
		register_post_type('episode',$args);
	}



	function mspm_rewrite_flush() {
		$this->mspm_register_series_object();
		$this->mspm_register_episode_object();

	    // ATTENTION: This is *only* done during plugin activation hook in this example!
	    // You should *NEVER EVER* do this on every page load!!
	    flush_rewrite_rules();
	}
	

}

endif;
