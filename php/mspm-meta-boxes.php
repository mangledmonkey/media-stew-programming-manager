<?php
if (!function_exists('is_admin')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

if (!class_exists("Media_Stew_Programming_Manager_Meta_Boxes")) :
	
class Media_Stew_Programming_Manager_Meta_Boxes {

	function mspm_admin_init() {
		add_action('admin_print_scripts', array(&$this,'mspm_admin_enqueue_scripts'));
		add_action('admin_print_styles', array(&$this,'mspm_admin_enqueue_styles'));
		add_action('add_meta_boxes', array(&$this, 'mspm_create_meta_boxes'));
		add_action('save_post', array(&$this, 'mspm_save_episode_meta_data'));
	}


	function mspm_admin_enqueue_scripts() {
		global $post;
		if($post->post_type == 'episode'|| 'series' && is_admin()) {
			wp_enqueue_script('anytime',MSPM_URL.'/js/anytime.c.js');
			wp_enqueue_script('jquery');
		}
	}
	
	function mspm_admin_enqueue_styles() {
		global $post;
		if($post->post_type == 'episode'|| 'series' && is_admin()) {
			wp_enqueue_style('anytime',MSPM_URL.'/css/anytime.c.css');
		}
	}

	function mspm_create_meta_boxes() {

		add_meta_box('episode_meta', 'Episode Details', array(&$this,'mspm_display_episode_meta_box'),'episode', 'normal','high');
		add_meta_box('series_meta', 'Series Details', array(&$this,'mspm_display_series_meta_box'),'series', 'normal','high');
	
	}
	
	function mspm_display_series_meta_box() {
		?>

		<div id="postcustomstuff">
			<p><strong>Describe this Series</strong></p>
			<table id="seriesmeta">
				<thead>
					<tr>
						<th>
							<label for="series_description">Series Synopsis</label>
						</th>
						<th class="left">
							<label for="series_airdate">Premiere Date</label>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php $this->mspm_textarea_description('series_description'); ?>
						</td>
						<td class="left">
							<?php echo $this->mspm_datetime_picker('series'); ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<?php
		//create a custom nonce for submit verification later
		echo '<input type="hidden" name="mspm_meta_nonce" value="' . wp_create_nonce(__FILE__) . '" />';
	}
	
	function mspm_display_episode_meta_box() {
		?>

		<div id="postcustomstuff">
		<p><strong>Define this Episode</strong></p>
		<table id="episodemeta">
			<thead>
				<tr>
					<th class="left">
						<label for="show_id">Parent Show</label>
					</th>
					<th>
						<label for="episode_season">Season</label>
					</th>
					<th>
						<label for="episode_number">Episode Number</label>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td id="newepisodeleft" class="left">
						<?php $this->mspm_dropdown_select_parent_page(); ?>
					</td>
					<td class="left">
						<?php $this->mspm_dropdown_select_season(4); ?>
					</td>
					<td class="left">
						<?php $this->mspm_dropdown_select_episode_number(12); ?>
					</td>
				</tr>
			</tbody>
		</table>
				
		<p><strong>Describe this Episode</strong></p>
	
		<table id="episode_codes">
			<thead>
				<tr>
					<th>
						<label for="episode_description">Episode Synopsis</label>
					</th>
					<th class="left">
						<label for"episode_airdate">Episode Airdate</label>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<?php $this->mspm_textarea_description('episode_description'); ?>
					</td>
					<td class="left">
						<?php echo $this->mspm_datetime_picker('episode'); ?>
					</td>
				</tr>
			</tbody>
		</table>

		<p><strong>Video Embed Codes</strong><p>
		<table id="episode_embed_url">
			<thead>
				<tr>
					<th class="left">
						<label for="episode_embed_url">Episode Vimeo URL</label>
					</th>
					<th class="left">
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="left">
						<?php $this->mspm_text_video_embed_url(); ?>
					</td>
					<td class="left">
					</td>
				<tr>
			</tbody>
		</table>
		<table id="episode_embed_codes">
			<thead>
				<tr>
					<th class="left">
						<label for="episode_embed_code">Episode Embed Code</label>
					</th>
					<th class="left">
						<label for="teaser_embed_code">Teaser Embed Code</label>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="left">
						<?php $this->mspm_textarea_embed_code('episode'); ?>
					</td>
					<td>	
						<?php $this->mspm_textarea_embed_code('teaser'); ?>
					</td>
				</tr>
			</tbody>
		</table>
		</div>
		<?php
		//create a custom nonce for submit verification later
		echo '<input type="hidden" name="mspm_meta_nonce" value="' . wp_create_nonce(__FILE__) . '" />';
	}


	/******************** BEGIN META BOX FIELDS **********************/

	function mspm_dropdown_select_parent_page() {
		global $post;
		$parent_id = $post->post_parent;
		// custom page type Series dropdown box
		$args = array(
			'depth' => 0,
			'child_of' => 0,
			'selected' => $parent_id,
			'echo' => 1,
			'hierarchical' => 0,
			'post_type' => 'series',
			'show_option_none' => '-- series --',
			'name' => 'parent_id',
			'id' => 'parent_id'
		);

		if (wp_dropdown_pages($args)) {
		} else {
			echo 'Sorry, no series found.';
		}
	}

	function mspm_dropdown_select_season($total) {
		global $post;
		$meta = get_post_meta($post->ID, 'episode_season', true);
		echo '<select name="episode_season" id="episode_season">';
		echo '	<option value>-- season --</option>';
		for($i=1;$i<=$total;$i++) {
			echo '<option value="'.$i.'" ', $i == $meta ? 'selected="selected"' : '' ,' >Season '.$i.'</option>';
		}
		echo '</select>';
	}


	function mspm_dropdown_select_episode_number($total) {
		global $post;
		$meta = get_post_meta($post->ID, 'episode_number',true);
		echo '<select name="episode_number" id="episode_number">';
		echo '	<option value>-- episode --</option>';
		for($i=1;$i<=$total;$i++) {
			echo '<option value="'.$i.'" ', $i == $meta ? "selected='selected'" : '' ,' >', $i < 10 ? '0' : '' , $i .'</option>';
					}
		echo '</select>';
	
	}

	function mspm_textarea_description($name) {
		global $post;
		$meta = get_post_meta($post->ID, $name, true);
		echo '<textarea name="'.$name.'" id="'.$name.'" rows="4" cols="100" tabindex="2"/>'.$meta.'</textarea>';
	}

	function mspm_text_video_embed_url() {
		global $post;
		$meta = get_post_meta($post->ID,'episode_video_url',true);
		echo '<input type="text" name="episode_video_url" id="episode_video_url" value="'.$meta.'" />';
	}

	function mspm_textarea_embed_code($term) {
		global $post;
		$ref = $term . '_embed_code';
		$meta = get_post_meta($post->ID, $ref, true);
		echo '<textarea name="'.$ref.'" id="'.$ref.'" rows="3" cols="75" tabindex="3">'.$meta.'</textarea>';
	}

	function mspm_datetime_picker($post_type) {
		global $post;
		if ($post_type == 'episode') {
			$name = 'episode_airdate';
		} else if ($post_type == 'series') {
			$name = 'series_airedate';
		}
		$meta = get_post_meta($post->ID, $name, true);
		?>
			<input type="text" id="<?php echo $name; ?>" name="<?php echo $name; ?>" value="<?php echo $meta; ?>" size="20" />
			<script>
					jQuery("#<?php echo $name; ?>").AnyTime_picker(
       					 { format: "%Y-%m-%d %H:%i:%s %@",
				        	  formatUtcOffset: "%: (%@)",
					          hideInput: false,
					          placement: "popup" } );
			</script>
			<style type="text/css">
			#<?php echo $name; ?> { background-image:url("<?php echo MSPM_URL.'/img/clock.png' ?>");
			    background-position:right center; background-repeat:no-repeat;
			    border:1px solid #FFC030;color:#3090C0;font-weight:bold}
			  #AnyTime--<?php echo $name; ?> {background-color:#EFEFEF;border:1px solid #CCC}
			  #AnyTime--<?php echo $name; ?> * {font-weight:bold}
			  #AnyTime--<?php echo $name; ?> .AnyTime-btn {background-color:#F9F9FC;
			    border:1px solid #CCC;color:#3090C0}
			  #AnyTime--<?php echo $name; ?> .AnyTime-cur-btn {background-color:#FCF9F6;
			      border:1px solid #FFC030;color:#FFC030}
			  #AnyTime--<?php echo $name; ?> .AnyTime-focus-btn {border-style:dotted}
			  #AnyTime--<?php echo $name; ?> .AnyTime-lbl {color:black}
			  #AnyTime--<?php echo $name; ?> .AnyTime-hdr {background-color:#FFC030;color:white}
</style>
			
		<?php
	}
	
	/******************** END META BOX FIELDS **********************/

	/******************** BEGIN META BOX SAVE **********************/
	

	function mspm_save_episode_meta_data($post_id) {
		global $wpdb;
		$post_type_object = get_post_type_object($_POST['post_type']);

		if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)		//check autosave
		|| (!isset($_POST['post_ID']) || $post_id != $_POST['post_ID'])	// check revision
		|| (!wp_verify_nonce($_POST['mspm_meta_nonce'],__FILE__))   //verify nonce
		|| (!current_user_can($post_type_object->cap->edit_post, $post_id))) {	//check permission
		       return $post_id;
		}
		
		// save each custom Series meta field
		if ($_POST['post_type'] == "series") {
			if(isset($_POST['series_description']) ) {
				$data = $_POST['series_description'];
				$data = htmlspecialchars($data);
				update_post_meta($post_id, 'series_description', $data);
			}
			if(isset($_POST['series_airdate']) ) {
				$data = $_POST['series_airdate'];
				update_post_meta($post_id, 'series_airdate', $data);
			}
		}

		// save each custom Episode meta field
		if ($_POST['post_type'] == "episode") {
			if(isset($_POST['parent_id'])) {
				$parent_id = $_POST['parent_id'];
				$wpdb->get_var( $wpdb->prepare("UPDATE $wpdb->posts SET post_parent = %d WHERE ID = %d", $parent_id, $post_id));
			}
			if(isset($_POST['episode_season'])) {
				$data = $_POST['episode_season'];
				update_post_meta($post_id, 'episode_season', $data);
			}
			if(isset($_POST['episode_number'])) {
				$data = $_POST['episode_number'];
				update_post_meta($post_id, 'episode_number', $data);
			}
			if(isset($_POST['episode_description'])) {
				$data = $_POST['episode_description'];
				$data = htmlspecialchars($data);
				update_post_meta($post_id, 'episode_description', $data);
			}
			if(isset($_POST['episode_video_url'])) {
				$data = $_POST['episode_video_url'];
				update_post_meta($post_id, 'episode_video_url', $data);	
			}
			if(isset($_POST['episode_embed_code'])) {
				$data = $_POST['episode_embed_code'];
				$data = wpautop($data);
				update_post_meta($post_id, 'episode_embed_code', $data);	
			}
			if(isset($_POST['teaser_embed_code'])) {
				$data = $_POST['teaser_embed_code'];
				$data = wpautop($data);
				update_post_meta($post_id, 'teaser_embed_code', $data);	
			}
			if(isset($_POST['episode_airdate'])) {
				$data = $_POST['episode_airdate'];
				update_post_meta($post_id, 'episode_airdate', $data);
			}
		}

	
	}

}
/********************* END VALIDATION ***********************/

endif;

?>
