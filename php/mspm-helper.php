<?php
if (!function_exists('is_admin')) {
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

if (!class_exists("Media_Stew_Programming_Manager_Helper")) :

class Media_Stew_Programming_Manager_Helper {

	function mspm_admin_init() {
		global $mspm;

		add_action('rewrite_rules_array',array(&$this,'mspm_login_url_rewrite'));
		add_filter('default_content', array(&$this,'mspm_default_post_content') );
	}

	
	// add plugin menu page
	function mspm_add_pages() {
		global $mspm;
		$mspm->mspm_options_pages = add_menu_page('Media Stew Programming Manager', 'MS Manager','manage_options', 'mspm', array(&$this,'mspm_manage_page'), MSPM_URL.'/img/MediaStewIcon.png' );
		$mspm->mspm_options_pages = add_submenu_page('mspm', 'Media Stew Programming Manager Options', 'Options', 'manage_options', 'mspm_options', array(&$this,'mspm_menu_page') );
		
	}

	function mspm_menu_page() {
		include('mspm-options.php');
	}

	function mspm_sanitize_options($input) {
		// rewrite_login must be either 1 or 0
		$input['rewrite_login'] = ( $input['rewrite_login'] == 1 ? 1 : 0 );
		
		// rewrite_login_value must be plain text like a post slug
		$input['rewrite_login_value'] = sanitize_title( $input['rewrite_login_value'] );
		
		return $input;
	}
	
	function mspm_login_url_rewrite() {
		$options = get_option('mspm_options');
		$rewrite = $options['rewrite_login'];
		$rewrite_value = $options['rewrite_login_value'];

		if ($rewrite == 1 ) {
			global $wp_rewrite;
			add_rewrite_rule($rewrite_value, 'wp-login.php', 'top');
		}
	}

	function mspm_default_post_content($content) {
	
		global $post_type;

		switch ($post_type) {
		case 'series':
			$content = '[mspm_header][mspm_list]';
			break;
		case 'episode':
			$content = '[mspm_player][mspm_list]';
			break;
		default:
			$content = '';
			break;
		}
		
		return $content;
		
	}

	// WISHLIST MEMBER HOOKS



}// End Media_Stew_Programming_Manager Class

endif;




?>
