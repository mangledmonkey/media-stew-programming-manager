<?php
if (!function_exists('is_admin')){
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

if (!class_exists("Media_Stew_Programming_Manager_Shortcodes")) :

class Media_Stew_Programming_Manager_Shortcodes {

	function mspm_init() {
		add_shortcode('mspm_header', array(&$this,'mspm_header'));
		add_shortcode('mspm_player', array(&$this,'mspm_player'));
		add_shortcode('mspm_list', array(&$this,'mspm_list'));
		add_shortcode('mspm_series_list', array(&$this,'mspm_series_list'));
		add_shortcode('mspm_latest_episodes', array(&$this,'mspm_latest_episodes'));
		add_action('wp', array(&$this,'mspm_enqueue_styles'));
		//add_image_size('MSPM Banner',960,300,true);
	}

	function mspm_enqueue_styles() {
		global $post;
		$pattern = get_shortcode_regex();
		preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches );

		// enqueue styles
		if( is_array( $matches ) && array_key_exists( 2, $matches ) && in_array( 'mspm_header'|| 'mspm_player'|| 'mspm_list' || 'mspm_series_list', $matches[2] ) )
		{
			// shortcode is being used
			wp_enqueue_style('mspm-style', MSPM_URL.'/css/style.css'); 
		}

		// enqueue scripts
		if (is_array( $matches ) && array_key_exists( 2, $matches ) && in_array( 'mspm_latest_episodes', $matches[2] ) )
		{
			// shorcode is being used
			wp_enqueue_script('bootstrap-carousel', MSPM_URL.'/js/bootstrap-carousel.js');
			add_action('wp_head',array(&$this,'mspm_custom_head_js'));
		}
	}

	function mspm_custom_head_js() {
		echo "<script type='text/javascript'>//<![CDATA[ 
				$(window).load(function(){
				$('#myCarousel').carousel('cycle');
			});//]]>  
			</script>";
	}

	// [mspm_header]
	function mspm_header($atts,$content=null,$code="") {
		global $post;
		$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full',false);
		$description = get_post_meta($post->ID,'series_description',true);
		?>

		<section class="mspm-header" style="background: url('<?php echo $thumbnail[0]; ?>') no-repeat top left #000">
		        <div class="mspm-header-title"><h2><?php echo the_title(); ?></h2></div>
			<div class="mspm-header-desc"><p><?php echo $description; ?></p></div>
		</section>

		<?php

	}
	
	// [mspm_player]
	function mspm_player($atts,$content=null,$code="") {
		global $post;
		$description = get_post_meta($post->ID,'episode_description',true);
		$aired = $this->mspm_check_release_date($post->ID,'episode_airdate');
		$airdate = $this->mspm_get_friendly_airdate($post->ID,'episode_airdate');
		
		?>
		<article class="mspm-player">
			<section class="mspm-pl-screen">
				<?php echo $this->mspm_get_video_embed(); ?>
		        </section>
			<article class="mspm-pl-desc">
				<section class="mspm-pl-desc-eif">
									<a href="<?php echo $episode->guid; ?>"><?php echo get_the_post_thumbnail($episode->ID, array(134,75),true); ?></a>
				</section>
				<article class="mspm-pl-desc-ef">
					<header>	
						<h2><?php echo the_title(); ?></h2>
						<p><span<?php if (!$aired) echo ' class="not-released"'; ?> ><?php echo $airdate; ?></span><br />
							<?php echo $this->mspm_is_aired_text($aired); ?>
							
						</p>
					</header>
					<p><?php echo $description; ?></p>
				</article>
			</article>
		</article>

		<?php
	}

	// [mspm_list]
	function mspm_list($atts,$content=null,$code="") {
		global $post;
		$post_parent = $post->post_type == 'episode' ? $post->post_parent : $post->ID;
		$series_args = array(
			'numberposts' => -1,
			'post_type' => 'series',
			'orderby' => 'post_title',
			'order' => 'ASC'
		);
		$episodes_args = array(
			'numberposts' => -1,
			'post_type' => 'episode',
			'post_parent' => $post_parent,
			'meta_key' => 'episode_number',
			'orderby' => 'episode_number',
			'order' => 'ASC'
		);	
		$series = get_posts($series_args);
		$episodes = get_posts($episodes_args);
		$shows_page = get_page_by_title('Shows');

		$is_single_episode = count($episodes) == 1;
		?>
		<section class="mspm-list">
			<section class="mspm-series">
				<h3>Series</h3>
				<ul>
					<?php foreach( $series as $show ){ ?>
					<li<?php echo $show->ID == $post->post_parent || $show->ID == $post->ID ? ' class="mspm-sel-s"' : '' ?> onclick="document.location.href='<?php echo get_permalink($show->ID); ?>'"><?php echo $show->post_title; ?></li>
					<?php } ?>
				</ul>	
				<p><a href="<?php echo get_permalink($shows_page->ID); ?>" class="mspm-arrow-btn">See all &rarr;</a></p>

			</section>
			<section class="mspm-episodes">
				<h3>Episodes</h3>
				<section class="mspm-el">
				<?php if ($episodes) { ?>
					<ul>
					<?php foreach( $episodes as $episode ){
					// if episode is current page, skip
					if ($episode->ID == $post->ID) {
						if ($is_single_episode) 
							echo '<p class="mspm-none">There are no other episodes available for this series.</p>';
							
						} else { 

						$description = get_post_meta($episode->ID, 'episode_description', true);
						$aired = $this->mspm_check_release_date($episode->ID, 'episode_airdate');
						 ?>
						<li <?php echo $episode->ID == $post->ID ? 'class="mspm-sel-e"' : ''; ?>>
							<article>
								<section class="mspm-eif">
									<a href="<?php echo get_permalink($episode->ID); ?>"><?php echo get_the_post_thumbnail($episode->ID, array(134,75),true); ?></a>
								</section>
								<section class="mspm-ef">
									<header>
										<hgroup>
											<h4><a href="<?php echo get_permalink($episode->ID); ?>"><?php echo $episode->post_title; ?></a></h4>
											<p>
											<span<?php if (!$aired) echo ' class="not-released"'; ?> >
												<?php echo $this->mspm_get_friendly_airdate($episode->ID,'episode_airdate'); ?><br />
											</span>
											<?php echo $this->mspm_is_aired_text($aired); ?>
											</p>
										</hgroup>
									</header>
									<p><?php echo substr($description,0,155). '...'; ?></p>
								</section>
								<div class="spacer"></div>
							</article>
						</li>
					<?php  }} ?>
					</ul>
				<?php } else { ?>
					<p class="mspm-none">This series is in production but there are no episodes available at this time.</p>
				<?php  } ?>
				</section>
			</section>
			<div class="spacer"></div>
		</section>
		<?php
	
	}

	function mspm_series_list() {
		$series_args = array(
			'numberposts' => -1,
			'post_type' => 'series',
			'orderby' => 'post_title',
			'order' => 'ASC'
		);
		$series = get_posts($series_args);

		?>
		<article class="mspm-sl-body">
			<header class="header">
			</header>
			<section class="content">

			<?php foreach ($series as $show ) { 
				$description = get_post_meta($show->ID,'series_description',true);
			?>
			<article class="show">
					<section class="show-thumb">
						<a href="<?php echo get_permalink($show->ID); ?>"><?php echo get_the_post_thumbnail($show->ID, array(125,222),true); ?></a>
					</section>
					<section class="show-desc">
						<header>
							<h2><a href="<?php echo get_permalink($show->ID); ?>"><?php echo $show->post_title; ?></a></h2>
						</header>
						<p><?php echo substr($description,0,160); ?></p>
					</section>
				</article>

			<?php } ?>
			</section>
			
		</article>
		<?php
	
	}

	function mspm_latest_episodes() {
		$series_args = array(
			'numberposts' => -1,
			'post_type' => 'series',
			'orderby' => 'post_title',
			'order' => 'ASC'
		);
		$series = get_posts($series_args);
		$carousel_count = 0;
		?>
		
		<div id="mspm_latest_episodes">
			<article>
				<header>
					<p>
						<a class="btn btn-small" href="#myCarousel" data-slide="prev"><i class="icon-arrow-left"></i></a>
						<a class="btn btn-small" href="#myCarousel" data-slide="next"><i class="icon-arrow-right"></i></a>
						<a href="<?php echo get_permalink( get_page_by_path('shows') ); ?>" class="btn btn-small">All Shows</a>
					</p>
					<h2>LATEST EPISODES</h2>
				</header>
			<div id="myCarousel" class="carousel slide">
			<div class="carousel-inner">
				<div class="item active">

				<ul> 
			<?php foreach ($series as $show) {
				$feature_episode = $this->mspm_get_feature_episode($show);
				$aired = $this->mspm_check_release_date($feature_episode->ID,'episode_airdate');
				$airdate = $this->mspm_get_friendly_airdate($feature_episode->ID,'episode_airdate');
				$episode_link = get_permalink($feature_episode->ID);
				if ($feature_episode) {
			 ?>
					<li>
						<section>
							<header>
								<div class="feature-thumbnail">
									<a href="<?php echo $episode_link; ?>"><?php echo get_the_post_thumbnail($feature_episode->ID, array(200,112),true); ?></a>
			 					</div>
									<h3><a href="<?php echo $episode_link; ?>"><?php echo $feature_episode->post_title; ?></a></h3>
								<span class="series-title"><a href="<?php echo get_permalink($show->ID); ?>"><?php echo $show->post_title; ?></a></span>
							<?php if (!$aired) { ?>
								<p class="not-released"><?php echo $airdate; ?><span><?php echo $this->mspm_is_aired_text($aired); ?></span></p>
							<?php } ?>
							</header>
							<p><?php echo substr(get_post_meta($feature_episode->ID,'episode_description',true),0,100); ?>...</p>
						</section>	
					</li>
	
<?php $carousel_count++;
if ($carousel_count % 4 == 0 && count($series) - 1 > $carousel_count) {
	echo '</ul>';
	echo '</div>';
	echo '<div class="item">';
	echo '<ul>';
}
				} } ?>
				</ul>
			
				</div><!-- end item -->
				</div><!-- end carousel-inner -->
			</div><!-- end myCarousel -->
			</article>
			<div class="spacer"></div>
		</div>
		<?php	
	}

	// ####### END SHORTCODES ######## //
	
	
	// ####### BEGIN SUPPORT FUNCTIONS ######## //

	function mspm_get_friendly_airdate($id,$field) {
		$dbdate = get_post_meta($id,$field,true);
		if ($this->mspm_check_release_date($id,$field)) {
			$airdate = date("M j\, Y", strtotime($dbdate));
		} else {
			$airdate = date("D M j \a\\t G:i \M\T", strtotime($dbdate));
		}

		return $airdate;
	}

	function mspm_check_release_date($id,$field) {
		$released = false;
		$dbdate = strtotime(get_post_meta($id,$field,true));
		$curdate = time();

		if ($dbdate < $curdate)
			$released = true;

		return $released;
	}

	function mspm_is_aired_text($aired) {
		$released = 'air date';
		if ($aired)
			$released = 'aired';

		return $released;
	}


	function mspm_get_video_embed($width='900') {
		global $post;
		$url = get_post_meta($post->ID,'episode_video_url',true);
		
		if (!$url) {
			echo get_post_meta($post->ID,'episode_embed_code',true);
		} else {
			// oEmbed
			$oembed_endpoint = 'http://vimeo.com/api/oembed';
			// Grab the video url from the url, or use default
			$video_url = ($_GET['url']) ? $_GET['url'] : $url;
			//create the URLs
			$xml_url = $oembed_endpoint. '.xml?url=' .rawurlencode($video_url) . '&width=' . $width;
			// Load in the oEmbed XML
			$oembed = simplexml_load_string($this->mspm_vimeo_curl_get($xml_url));

			echo html_entity_decode($oembed->html);
		}
	}

	function mspm_vimeo_curl_get($url) {
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		$return = curl_exec($curl);
		curl_close($curl);
		return $return;
	}

	function mspm_get_feature_episode($series) {
		$latest = $this->mspm_get_latest_series_episode($series,'<=');
		if (!$latest) {
			$unreleased = $this->mspm_get_latest_series_episode($series,'>');
			if (!$unreleased) {
				return false;
			} else {
				return $unreleased;
			}
		} else {
			return $latest;
		}
	}
	
	function mspm_get_latest_series_episode($series,$compare) {
		$parent_id = $series->ID;
		$date = date("Y-m-d %H:i:s");
		if ($compare == '>' || '>=') {
			$order = 'ASC';
		} else {
			$order = 'DESC';
		}
		$args = array(
			'post_type' => 'episode',
			'post_parent' => $parent_id,
			'meta_key' => 'episode_airdate',
			'orderby' => 'meta_value',
			'meta_compare' => $compare,
			'meta_value' => $date,
			'order' => $order
			);
		$episodes = get_posts($args);
		$feature = $episodes[0];
		if (!$feature) {
			return false;
		} else {
			return $feature;
		}
	}

}

endif;
?>
