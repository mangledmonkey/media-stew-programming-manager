<?php
if (!function_exists('is_admin')) {
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

if (!class_exists("Media_Stew_Programming_Manager_Taxonomies")) :

class Media_Stew_Programming_Manager_Taxonomies {


	function mspm_activate() {
		//$this->msmp_register_show_taxonomies();
		$this->mspm_register_season_taxonomies();
	}	

	function mspm_admin_init() {
		
//		add_action('admin_menu', 'mspm_create_season_meta_box');
//		add_action('save_post', 'mspm_save_season');
	}

	function mspm_register_season_taxonomies() {
		if (!taxonomy_exists('season')) {
			$labels = array(
				'name' => _x('Seasons', 'taxonomy general name'),
				'singular_name' => _x('Season', 'taxonomy singular name'),
				'search_items' => __('Search Genres'),
				'all_items' => __('All Seasons'),
				'parent_item' => __('Parent Season'),
				'parent_item_colon' => __('Parent Season:'),
				'edit_item' => __('Edit Season'),
				'update_item' => __('Update Season'),
				'add_new_item' => __('Add New Season'),
				'new_item_name' => __('New Season Name')
			);

			$args = array(
				'heirarchical' => false,
				'labels' => $labels,
				'show_ui' => true,
				'query_var' => 'season',
				'rewrite' => array('slug' => 'season')
			);		
			register_taxonomy('season',null,$args);

			for ($i=1;$i<=10;$i++) {
				wp_insert_term(
					'Season '.$i,
					'season',
					array(
						'slug' => 'season'.$i,
					)
				);
			}
		}

	
	}

}

endif;
?>
