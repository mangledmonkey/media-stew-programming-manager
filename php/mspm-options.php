<?php
if (!function_exists('is_admin')) {
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

if (!is_admin()) exit();

global $mspm;

//display the admin options page
if (!current_user_can('manage_options'))  {
	wp_die( __('You do not have sufficient permissions to access this page.') );
}
?>
<div class="wrap">
	<h2>Media Stew Programming Manager</h2>
	<form method="post" action="options.php">
		<?php settings_fields('mspm_general_options'); ?>
		<?php $options = get_option('mspm_options'); ?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><label for="rewrite_login" name="rewrite_login">Rewrite Login Page URL</label></th>
				<td>
					<input name="mspm_options[rewrite_login]" type="checkbox" value="1" <?php checked('1', $options['rewrite_login']); ?> />
					<input type="text" name="mspm_options[rewrite_login_value]" value="<?php echo $options['rewrite_login_value']; ?>" />
					<p>example: login (produces: domain.com/login)</p>
					<p>Visit Settings->Permalinks to flush rewrite caching.</p>
				</td>
			</tr>
		</table>
		<p class="submit">
			<input class="button-primary" type="submit" value="<?php _e('Save Changes'); ?>" />
		</p>
	</form>

	<h3>Shortcodes</h3>

	<table>
		<thead>
			<tr valign="top">
				<th>Shortcode</th>
				<th>Definition</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>[mspm_series_list]</td>
				<td></td>
			</tr>
			<tr>
				<td>[mspm_player]</td>
				<td></td>
			</tr>
			<tr>
				<td>[mspm_list]</td>
				<td></td>
			</tr>
			<tr>
				<td>[mspm_latest_episode]</td>
				<td></td>
			</tr>
			
		</tbody>
	</table>
		
</div>
<?php


//validate our options
function mspm_options_validate($input) {
	$newinput['text_string'] = trim($input['text_string']);
	if(!preg_match('/^[a-z0-9]{32}$/i', $newinput['text_string'])) {
		$newinput['text_string'] = '';
	}
	return $newinput;
}





?>
