=== Plugin Name ===
Contributors: Brandon West
Donate link: 
Tags: video, episode, episodic, scheduling, schedule, media, manager, management, programming, distribution
Requires at least: 3.1 
Tested up to: 3.1
Stable tag: 1.0

Schedule and manage episodic video content releases with social features.

== Description ==

This plugin adds an unlimited show and episodic media programming manager for online scheduled video content releases and distribution with social user intraction features and widgets.

== Installation ==

1. Upload 'media-stew-programming-manager/' folder to the '/wp-content/plugins' directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the directory of the stable readme.txt, so in this case, `/tags/4.3/screenshot-1.png` (or jpg, jpeg, gif)
2. This is the second screen shot

== Changelog ==

= 0.3 =
- Added Media Stew icon to admin menu
- Larger video player section
- Vimeo player oEmbed
- Added Latest Episodes widget

= 0.2 =
- Added login page friendly url rewrite to plugin options page.
- Using minified date/time picker javascript


== Upgrade Notice ==

= 0.3 =
New features and larger video player.

= 0.2 =
This is a slight improvement on 0.1.

= 0.1 =
This is the first stable version.
